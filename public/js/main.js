$(document).ready(function () {

    $('body').on('click', '.category', function () {

        $.ajax({
            type: 'get',
            url: '/default/index/changecategory',
            data: 'idcat=' + this.id,
            success: function (data) {
                $(".demo").html(data);

            }
        });

        return false;

    });

    $('.demo').on('click', '.addtoList', function () {

        $.ajax({
            type: 'get',
            url: '/mylist/addtomylist',
            data: 'idproduct=' + this.id,
            success: function (data) {
                data_result = JSON.parse(data);

                if (data_result.status == 'ok') {
                    alert('Product successfully added');
                    $("#countprod").html(data_result.count);

                } else if (data_result.status = 'false') {
                    alert('Something wrong please, try later');

                }

            }
        });

        return false;

    });

    $('.deleteitem').click(function () {
        if (confirm("Are you sure ?")) {

            $.ajax({
                type: 'get',
                url: '/mylist/deleteitem',
                data: 'idproduct=' + this.id,
                success: function (data) {
                    if (data == 'ok') {
                        location.href = "/mylist";
                    }
                }
            });

        }

        return false;
    })

    $('.clearall').click(function () {
        if (confirm("Are you sure ?")) {

            $.ajax({
                type: 'get',
                url: '/mylist/deleteallitems',
                success: function (data) {
                    if (data == 'ok') {
                        location.href = "/";
                    }
                }
            });

        }

        return false;
    })

});

