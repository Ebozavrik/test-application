
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Мар 24 2013 г., 12:39
-- Версия сервера: 5.1.66
-- Версия PHP: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `u841630051_mylistba`
--

-- --------------------------------------------------------

--
-- Структура таблицы `site_myproductlist`
--

CREATE TABLE IF NOT EXISTS `site_myproductlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=58 ;

--
-- Дамп данных таблицы `site_myproductlist`
--

INSERT INTO `site_myproductlist` (`id`, `product_id`) VALUES
(55, 4),
(56, 4),
(57, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `site_productcategory`
--

CREATE TABLE IF NOT EXISTS `site_productcategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_category` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `site_productcategory`
--

INSERT INTO `site_productcategory` (`id`, `name_category`) VALUES
(1, 'food'),
(2, 'drink'),
(3, 'household goods');

-- --------------------------------------------------------

--
-- Структура таблицы `site_productlist`
--

CREATE TABLE IF NOT EXISTS `site_productlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `site_productlist`
--

INSERT INTO `site_productlist` (`id`, `cat_id`, `name`, `image`) VALUES
(2, 1, 'pizza', '1364133991_pizza_slice_128.png'),
(3, 2, 'Coca-Cola', '1364134145_Coca-Cola-Zero_128.png'),
(4, 1, 'Milk', '1364134177_Glass_of_Milk.png'),
(5, 1, 'cheese', '1364134203_moon.png'),
(6, 3, 'Shampoo', 'phpThumb_generated_thumbnailjpg.jpg'),
(7, 3, 'Washing powder', '548547.jpg'),
(8, 3, 'soap', '1364134418_Paper Street Soap Co..png'),
(9, 2, 'Fanta', '1364134456_Fanta-1_128.png'),
(10, 2, 'Pepsi', '1364134489_Pepsi-Classic_128.png');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
