<?php

class MylistController extends Zend_Controller_Action
{

    public function init ()
    {
        $this->view->countProduct = Application_Model_Myproductlist::getInstance()->getCountProduct();
    }

    public function indexAction ()
    {
        $products = Application_Model_Myproductlist::getInstance()->getMyProducts();

        $this->view->products = $products;

    }


    public function addtomylistAction ()
    {
        $forReturn = array();

        $product = Application_Model_Myproductlist::getInstance()->addproduct($this->_getParam('idproduct'));

        if (is_int($product)) {
            $forReturn['status'] = 'ok';
            $forReturn['count']  = $product;
        } else {
            $forReturn['status'] = 'false';
        }
        echo Zend_Json::encode($forReturn);

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
    }

    public function deleteitemAction ()
    {
        Application_Model_Myproductlist::getInstance()->deleteproduct($this->_getParam('idproduct'));
        echo 'ok';
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
    }

    public function deleteallitemsAction ()
    {
        Application_Model_Myproductlist::getInstance()->deleteAllproduct();
        echo 'ok';
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

    }


}

