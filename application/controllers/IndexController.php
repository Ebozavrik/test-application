<?php

class IndexController extends Zend_Controller_Action
{

    public function init ()
    {
        $this->view->countProduct = Application_Model_Myproductlist::getInstance()->getCountProduct();
    }

    public function indexAction ()
    {
        $productList     = Application_Model_Productlist::getInstance()->getAllProducts();
        $productCategory = Application_Model_Produccategory::getInstance()->getAllProducts();


        $this->view->products         = $productList;
        $this->view->productscategory = $productCategory;


        // action body
    }


    public function changecategoryAction ()
    {

        $productList = Application_Model_Productlist::getInstance()->getProductsByCategory($this->_getParam('idcat'));

        $this->view->products = $productList;

        $this->_helper->layout()->disableLayout();

        return $this->render('/changecategory');

    }


}

