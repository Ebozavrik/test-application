<?php

class Application_Model_Myproductlist extends Zend_Db_Table
{

    /**
     * The default table name.
     *
     * @var string
     */
    protected $_name = 'site_myproductlist';
    /**
     * The default primary key.
     *
     * @var array
     */
    protected $_primary = array( 'id' );
    /**
     * Whether to use Autoincrement primary key.
     *
     * @var boolean
     */
    protected $_sequence = true;

    /**
     * Singleton instance.
     *
     * @var St_Model_Layout_Pages
     */
    protected static $_instance = null;

    /**
     * Singleton instance
     *
     * @return Application_Model_Myproductlist
     */
    public static function getInstance ()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /*
     * Get All Products
     */
    public function getAllProducts ()
    {
        $select = $this->select();

        return $this->fetchAll($select);
    }

    /*
     * Add product
     */
    public function addproduct ($id)
    {
        $data['product_id'] = $id;
        $this->insert($data);

        $select = $this->fetchAll('SELECT COUNT(*) FROM ' . $this->_name)->toArray();

        return count($select);

    }

    /*
     * Get count product
     * for header
     */
    public function getCountProduct ()
    {
        $select = $this->fetchAll('SELECT COUNT(*) FROM ' . $this->_name)->toArray();

        return count($select);
    }


    /*
     * Get product
     * with sort
     */
    public function getMyProducts ()
    {
        $select = $this->select();
        $select->setIntegrityCheck(false);
        $select->from(array( 'p' => $this->_name ));
        $select->joinLeft(
            array( 'i' => 'site_productlist' ), 'p.product_id=i.id'
        );
        $select->order('i.cat_id');
        $select->order('p.product_id');


        $arrayForReturn = array();


        foreach ($this->fetchAll($select) as $value) {

            if (array_key_exists($value->product_id, $arrayForReturn)) {
                $arrayForReturn[$value->product_id]['count']++;

            } else {
                $arrayForReturn[$value->product_id] = array(
                    'id'     => $value->product_id,
                    'name'   => $value->name,
                    'cat_id' => $value->cat_id,
                    'image'  => $value->image,
                    'count'  => 1,
                );
            }
        }

        return $arrayForReturn;
    }

    /*
     * Delete product
     * from my list
     */
    public function deleteproduct ($idprod)
    {
        $where = $this->getAdapter()->quoteInto('product_id = ?', $idprod);

        return $this->delete($where);
    }

    /*
     * Delete items from list
     */
    public function deleteAllproduct ()
    {
        $where = $this->getAdapter()->quoteInto('1 = ?', 1);

        return $this->delete($where);
    }

}