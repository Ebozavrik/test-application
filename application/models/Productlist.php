<?php

class Application_Model_Productlist extends Zend_Db_Table
{

    /**
     * The default table name.
     *
     * @var string
     */
    protected $_name = 'site_productlist';
    /**
     * The default primary key.
     *
     * @var array
     */
    protected $_primary = array( 'id' );
    /**
     * Whether to use Autoincrement primary key.
     *
     * @var boolean
     */
    protected $_sequence = true; // Использование таблицы с автоинкрементным ключом

    /**
     * Singleton instance.
     *
     * @var St_Model_Layout_Pages
     */
    protected static $_instance = null;

    /**
     * Singleton instance
     *
     * @return Application_Model_Productlist
     */
    public static function getInstance ()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /*
     * Get All Products
     */
    public function getAllProducts ()
    {
        $select = $this->select();

        return $this->fetchAll($select);
    }

    /*
     * Sort by category
     */
    public function getProductsByCategory ($catid)
    {
        if ($catid != 0) {
            $select = $this->select()
                ->where('cat_id =?', $catid);
        } else {
            $select = $this->select();
        }


        return $this->fetchAll($select);
    }

}