<?php

class Application_Model_Produccategory extends Zend_Db_Table
{

    /**
     * The default table name.
     *
     * @var string
     */
    protected $_name = 'site_productcategory';
    /**
     * The default primary key.
     *
     * @var array
     */
    protected $_primary = array( 'id' );
    /**
     * Whether to use Autoincrement primary key.
     *
     * @var boolean
     */
    protected $_sequence = true; // Использование таблицы с автоинкрементным ключом

    /**
     * Singleton instance.
     *
     * @var St_Model_Layout_Pages
     */
    protected static $_instance = null;

    /**
     * Singleton instance
     *
     * @return Application_Model_Produccategory
     */
    public static function getInstance ()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /*
     * Get All Products
     */
    public function getAllProducts ()
    {
        $select = $this->select();

        return $this->fetchAll($select);
    }


    /*
     * Возвращает все опбликованные статьи
     *
     */

    public function getAllPub ($page_id, $ofset = null, $count = null)
    {
        $where = array();
        if (is_array($page_id)) {
            $where[] = $this->getAdapter()->quoteInto('pub= ?', 1);
        } else {
            $where = array(
                $this->getAdapter()->quoteInto('is_active= ?', 1),
                $this->getAdapter()->quoteInto('id_page= ?', (int)$page_id)
            );
        }

        $order = array( $this->getAdapter()->quoteInto('created_at DESC', null),
            $this->getAdapter()->quoteInto('name ASC', null)
        );

        return $this->fetchAll($where, $order, $count, $ofset);
    }

}